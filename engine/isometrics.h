#ifndef ISOMETRICS_H_
#define ISOMETRICS_H_

#include <SDL2/SDL.h>

struct Point {
    int x;
    int y;
};

void setupRect(SDL_Rect *rect, int x, int y, int w, int h);
void convertWorldToScreen(struct Point *point);
void convertScreenToWorld(struct Point *point);

#endif
