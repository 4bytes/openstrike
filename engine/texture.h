#ifndef TEXTURE_H_
#define TEXTURE_H_

typedef struct Texture
{
    int x;
    int y;
    int width;
    int height;
    double angle;
    SDL_Point *center;
    SDL_Rect *cliprect;
    SDL_RendererFlip fliptype;
    SDL_Texture *texture;
} Texture;

int loadTexture(Texture *texture, char *filename);
void textureInit(Texture *texture, int x, int y, double angle, SDL_Point *center,
                 SDL_Rect *cliprect, SDL_RendererFlip fliptype);
void textureRenderXYClip(Texture *texture, int x, int y, SDL_Rect *cliprect);
/* void textureRenderXYClipScale(Texture *texture, int x, int y, SDL_Rect *cliprect, */
/*                               float scale); */
void textureDelete(Texture *texture);

#endif
