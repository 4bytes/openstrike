#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>

#include "renderer.h"
#include "texture.h"

int loadTexture(Texture *texture, char *filename)
{
    SDL_Surface *tmpsurf = IMG_Load(filename);

    if(tmpsurf == NULL) {
        fprintf(stderr,
                "Texture error: Could not load image: %s! SDL_image Error: %s\n",
                filename, IMG_GetError());
        return 0;
    } else {
        texture->texture = SDL_CreateTextureFromSurface(getRenderer(), tmpsurf);

        if(texture->texture == NULL) {
            fprintf(stderr,
                    "Texture error: Could not load image: %s! SDL_image Error: %s\n",
                    filename, IMG_GetError());
            SDL_FreeSurface(tmpsurf);
            return 0;
        } else {
            texture->width = tmpsurf->w;
            texture->height = tmpsurf->h;
        }

        SDL_FreeSurface(tmpsurf);
        return 1;
    }

    return 0;
}

void textureInit(Texture *texture, int x, int y, double angle, SDL_Point *center,
                 SDL_Rect *cliprect, SDL_RendererFlip fliptype)
{
    texture->x = x;
    texture->y = y;
    texture->angle = angle;
    texture->center = center;
    texture->fliptype = fliptype;
    texture->cliprect = cliprect;
}

void textureRenderXYClip(Texture *texture, int x, int y, SDL_Rect *cliprect)
{
    if(texture == NULL) {
        fprintf(stderr, "Warning: passed texture is null!\n");
        return;
    }

    texture->x = x;
    texture->y = y;
    texture->cliprect = cliprect;
    SDL_Rect quad = {texture->x, texture->y, texture->width, texture->height };
    if(texture->cliprect != NULL) {
        quad.w = texture->cliprect->w;
        quad.h = texture->cliprect->h;
    }

    SDL_RenderCopyEx(getRenderer(), texture->texture, texture->cliprect,
                     &quad, texture->angle, texture->center, texture->fliptype);
}

/* void textureRenderXYClipScale(Texture *texture, int x, int y, SDL_Rect *cliprect, float scale) */
/* { */
/*     fprintf(stdout, "Not implemented yet!\n"); */
/* } */

void textureDelete(Texture *texture)
{
    if(texture != NULL) {
        if(texture->texture != NULL) {
            SDL_DestroyTexture(texture->texture);
        }
    }
}
