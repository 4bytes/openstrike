#include <sys/queue.h>
#include <stdio.h>
#include <stdlib.h>

#define WIDTH 16
#define HEIGHT 16

SLIST_HEAD(Wave, Tile);

struct Tile {
	int x, y;
	SLIST_ENTRY(Tile) tiles;
};

void print_wave(struct Wave *w)
{
	printf("Wave: ");
	struct Tile *t;
	SLIST_FOREACH(t, w, tiles) {
		printf("%d:%d ", t->x, t->y);
	}
	printf("\n");
}

struct Tile *Wave_list_append(struct Wave *w, int x, int y)
{
	if (x >= WIDTH || y >= HEIGHT) {
		return NULL;
	}

	if (!SLIST_EMPTY(w)) {
		struct Tile *t;
		SLIST_FOREACH(t, w, tiles) {
			if (t->x == x && t->y == y) {
				// duplicate
				return NULL;
			}
		}
	}

	struct Tile *my_t = malloc(sizeof(struct Tile));
	if (my_t == NULL) {
		// OOM
		return NULL;
	}
	my_t->x = x;
	my_t->y = y;
	SLIST_INSERT_HEAD(w, my_t, tiles);

	return my_t;
}

void for_each_wave(struct Wave *w, void (*cb)(struct Wave *w))
{
	struct Wave my_wave;
	SLIST_INIT(&my_wave);

	if (w == NULL) {
		Wave_list_append(&my_wave, 0, 0);
	} else {
		struct Tile *t;
		SLIST_FOREACH(t, w, tiles) {
			Wave_list_append(&my_wave, t->x + 1, t->y);
			Wave_list_append(&my_wave, t->x, t->y + 1);
		}
	}

	if (SLIST_EMPTY(&my_wave)) {
		return;
	}
	
	struct Tile *t;
	cb(&my_wave);

	for_each_wave(&my_wave, cb);

	while (!SLIST_EMPTY(&my_wave)) {
		struct Tile *my_t = SLIST_FIRST(&my_wave);
		SLIST_REMOVE_HEAD(&my_wave, tiles);
		free(my_t);
	}
}

int main()
{
	for_each_wave(NULL, print_wave);

	return 0;
}

