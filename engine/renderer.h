#ifndef __RENDERER_H
#define __RENDERER_H

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

void initRenderer(char *windowTitle);
SDL_Renderer *getRenderer();
SDL_Window *getWindow();
void closeRenderer();

#endif
