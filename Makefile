# Install
BIN = ss

# Flags
CFLAGS += -g3 -Wall -Wextra -Wpedantic -O2
LDFLAGS = -lm -lSDL2 -lSDL2_image

SRC = main.c engine/renderer.c engine/initclose.c engine/texture.c \
      engine/isometrics.c
OBJ = $(SRC:.c=.o)

$(BIN):
	@mkdir -p bin
	rm -f bin/$(BIN) $(OBJS)
	$(CC) $(SRC) $(CFLAGS) -o bin/$(BIN) $(LDFLAGS)

clean:
	rm -rf *.o main
