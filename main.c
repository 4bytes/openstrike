#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdbool.h>
#include "engine/renderer.h"
#include "engine/initclose.h"
#include "engine/isometrics.h"

#define MAPSIZE 50

#define ERR(fmt,...)                                                    \
    printf("[file: %s, func: %s, line: %d]: " fmt,                      \
           __FILE__,                                                    \
           __func__,                                                    \
           __LINE__,                                                    \
           __VA_ARGS__)

typedef struct Game
{
    SDL_Event event;
    int loopDone;
    struct Point cursor;

    /* map */
    struct Point scroll;
    struct Point mapScroll2Dpos;
    int scrollSpeed;
}Game;

Game game;

static struct Point pt[MAPSIZE][MAPSIZE];

void initialize()
{
    game.loopDone = 0;
    game.scroll.x = 0;
    game.scroll.y = 0;
    game.mapScroll2Dpos.x = 0;
    game.mapScroll2Dpos.y = 0;
    game.scrollSpeed = 5;
}

/* move to isometric */
void convertCamTo2D(Game *game, struct Point *camPos)
{
    struct Point pt;
    pt = game->scroll;

    /* 2d to iso */
    convertWorldToScreen(&pt);

    if(pt.x < 0) {
        pt.x = abs(pt.x);
    }
    else if(pt.x > 0) {
        pt.x = -abs(pt.x);
    }
    *camPos = pt;
}

void convertCamToIso(Game *game, struct Point *camPos)
{
    /* meditation was here */
    /* REVIEW */
    struct Point tmp_pt;

    tmp_pt = *camPos;

    ERR("DEBUG: camPos: %d x %d\n", tmp_pt.x, tmp_pt.y);

    tmp_pt.x = (int)tmp_pt.x * 2;

    if(tmp_pt.x < 0) {
        tmp_pt.x = abs((int)tmp_pt.x);
    } else if (tmp_pt.x > 0) {
        tmp_pt.x = -abs((int)tmp_pt.x);
    }

    /* iso to 2d */
    convertScreenToWorld(&tmp_pt);

    game->scroll.x = (int)tmp_pt.x;
    game->scroll.y = (int)tmp_pt.y;
}

void scrollMap()
{
    if(game.cursor.x < 2) {
        game.mapScroll2Dpos.x -= game.scrollSpeed;
        convertCamToIso(&game, &game.mapScroll2Dpos);
    }

    if(game.cursor.x > WINDOW_WIDTH - 2) {
        game.mapScroll2Dpos.x += game.scrollSpeed;
        convertCamToIso(&game, &game.mapScroll2Dpos);
    }

    if(game.cursor.y < 2) {
        game.mapScroll2Dpos.y += game.scrollSpeed;
        convertCamToIso(&game, &game.mapScroll2Dpos);
    }

    if(game.cursor.y > WINDOW_HEIGHT -2) {
        game.mapScroll2Dpos.y -= game.scrollSpeed;
        convertCamToIso(&game, &game.mapScroll2Dpos);
    }
}

void drawIsoDots()
{
    SDL_SetRenderDrawColor(getRenderer(), 0xff, 0xff, 0x00, 0xff);
    for(int i = 0; i < MAPSIZE; i++) {
        for(int j = 0; j < MAPSIZE; j++) {
            SDL_RenderDrawPoint(getRenderer(), pt[i][j].x, pt[i][j].y);
        }
    }

}

void drawLines() {

    for(int i = 0; i < MAPSIZE; i++) {
        for(int j = 0; j < MAPSIZE; j++) {
            pt[i][j].x = 32 * j + game.scroll.x;
            pt[i][j].y = 32 * i + game.scroll.y;
            convertWorldToScreen(&pt[i][j]);
        }
    }

    SDL_SetRenderDrawColor(getRenderer(), 0xac, 0xac, 0xac, 0x00);

    for(int i = 0; i < MAPSIZE; i++) {
        for(int j = 0; j < MAPSIZE; j++) {
            if(!(j + 1 >= MAPSIZE)) {
                SDL_RenderDrawLine(getRenderer(), pt[i][j].x, pt[i][j].y,
                                   pt[i][j+1].x, pt[i][j+1].y);
            }
            if(!(i + 1 >= MAPSIZE)) {
                SDL_RenderDrawLine(getRenderer(), pt[i][j].x, pt[i][j].y,
                                   pt[i+1][j].x, pt[i+1][j].y);
            }
        }
    }
}

void drawVerticals()
{
    ERR("dummy: %s\n", ":)");
}


void draw()
{
    SDL_SetRenderDrawColor(getRenderer(), 0x00, 0x00, 0x00, 0x00);
    SDL_RenderClear(getRenderer());

    /* drawIsoDots(); */
    /* DEBUG: print current mouse coordinates */
    /* printf("MOUSE: %d x %d\n", game.cursor.x, game.cursor.y); */

    drawLines();
    /* drawVerticals(); */

    SDL_RenderPresent(getRenderer());

    /* Do not be a CPU HOG! */
    SDL_Delay(10);
}

void update()
{
    SDL_GetMouseState(&game.cursor.x, &game.cursor.y);
    scrollMap();
}

void updateInput()
{
    while(SDL_PollEvent(&game.event) != 0) {
        switch(game.event.type) {

        case SDL_QUIT:
            game.loopDone = 1;
            break;

        case SDL_KEYUP:
            switch(game.event.key.keysym.sym) {
            case SDLK_ESCAPE:
                game.loopDone = 1;
                break;

            default: break;
            }
        default: break;
        }
    }
}

int main(void)
{
    initSDL("OpenStrike");
    initialize();

    /* hide system cursor */
    /* SDL_ShowCursor(0); */

	/* SDL_SetWindowGrab(getWindow(), SDL_TRUE); */
	/* SDL_WarpMouseInWindow(getWindow(), WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2); */

    while(!game.loopDone) {
        update();
        updateInput();
        draw();
    }

    closeDownSDL();

    return 0;
}
