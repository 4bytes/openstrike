#include <stdlib.h>
#include "map.h"
#include "../engine/isometrics.h"

struct MAP *initMap(int width, int height, int numLayers, int tileSize)
{
    struct MAP *map = malloc(sizeof(struct MAP));
    if(map == NULL) {
        fprintf(stderr, "initMap(): unable to allocate memory for map.\n");
        return NULL;
    }

    map->tiles = malloc(sizeof(struct TILE));
    if(map->tiles == NULL) {
        fprintf(stderr, "initMap(): unable to allocate memory for map tiles.\n");
        return NULL;
    }

    map->tiles->numRects = 0;
    map->tiles->texture = malloc(sizeof(struct Texture));
    if(map->tiles->texture == NULL) {
        fprintf(stderr, "initMap(): unable to allocate memory for map tiles texture.\n");
        return NULL;
    }

    map->tiles->texture->texture = NULL;
    map->tiles->rects = NULL;
    map->tiles->coords.x = 0;
    map->tiles->coords.y = 0;

    map->tileSize = tileSize;
    map->width = width;
    map->height = height;
    map->numLayers = numLayers;

    return map;
}

int loadMapTiles(struct MAP *map, char *filename, int tileWidth, int tileHeight)
{
    int w;
    int h;
    int numTilesX;
    int numTilesY;
    SDL_Rect tmpRect;

    int x = 0;
    int y = 0;
    int i = 0;

    if(map == NULL) {
        fprintf(stderr, "loadMapTiles(): map is null.\n");
        return -1;
    }

    if(loadTexture(map->tiles->texture, filename) == 0) {
        return -1;
    }

    w = map->tiles->texture->width;
    h = map->tiles->texture->height;

    if(w < tileWidth) {
        fprintf(stderr, "loadMapTiles(): texture width is smaller than the file width.\n");
        return -1;
    }

    if(h < tileHeight) {
        fprintf(stderr, "loadMapTiles(): texture height is smaller than the file height.\n");
        return -1;
    }

    map->tiles->numRects = 1;
    map->tiles->rects = (SDL_Rect *) malloc(sizeof(SDL_Rect) * map->tiles->numRects);

    while(1) {
        /* setup the clip rectangle */
        setupRect(&tmpRect, x, y, tileWidth, tileHeight);

        /* copy rectangle */
        map->tiles->rects[i] = tmpRect;

        /* go to the next image */
        x += tileWidth;

        /* if the x position passed the width of the texture */
        if(x >= w) {
            /* reset the x position */
            x = 0;

            /* go to the next row in the image */
            y += tileHeight;

            if(y >= h) {
                /* break out of the loop. No more clip rectangles to create */
                break;
            }
        }
        i++;
    }

    return 1;
}

void drawMap(struct MAP *map)
{
    int i, j;
    struct Point point;
    int tile = 0;

    if(map == NULL) {
        fprintf(stderr, "drawMap(): map is NULL.\n");
        return;
    }

    for(i = 0; i < map->height; i++) {
        for(j = 0; j < map->width; j++) {
            point.x = j * map->tileSize;
            point.y = i * map->tileSize;
            /* convertWorldToScreen(&point); */
            /* printf("x = %d, y = %d\n", point.x, point.y); */

            textureRenderXYClip(map->tiles->texture,
                                point.x,
                                point.y,
                                &map->tiles->rects[tile]);
        }
    }
}
