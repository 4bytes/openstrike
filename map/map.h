#ifndef MAP_H_
#define MAP_H_

#include <SDL2/SDL.h>
#include "../engine/texture.h"
#include "../engine/isometrics.h"

struct TILE {
    int numRects;
    Texture *texture;
    SDL_Rect *rects;
    struct Point coords;
};

struct MAP {
    int width;
    int height;
    int tileSize;
    int numLayers;
    struct TILE *tiles;
};

struct MAP *initMap(int width, int height, int numLayers, int tileSize);
int loadMapTiles(struct MAP *map, char *filename, int tileWidth, int tileHeight);
void drawMap(struct MAP *map);

#endif
